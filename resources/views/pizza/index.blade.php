@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Pizzas</div>

                <div class="panel-body">
                    <a href="/pizza/create">Crear una pizza</a>
                    <table class="table">
                        <thead>
                            <th>Nombre</th>
                        </thead>
                        @foreach ($pizzas as $pizza)
                            <tr>
                                <td>{{$pizza->name}}</td>
                                <td>{{$pizza->users->name}}</td>
                                <td><a href="/pizzas/{{$pizza->id}}">Ver más...</a></td>
                                <td>
                                    <form class="" action="/pizzas/{{$pizza->id}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" value="Borrar" class="btn btn-danger">
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    </table>
                    {{ $pizzas->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
