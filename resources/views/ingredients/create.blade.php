@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ingrediente</div>

                <div class="panel-body">
                    <p>Creacion de un ingrediente</p>
                    <div class="form">
                    <form  action="/pizzas" method="post">
                    {{ csrf_field() }}


                    <div class="form-group">
                        <label>Nombre: </label>
                        <input type="text" name="name" value="{{ old('name') }}">
                        {{ $errors->first('name') }}
                    </div>
                    <div class="form-group">
                        <label>Tipo: </label>
                        <select class="form-control" name="type_id">
                            @foreach ($ingredients as $ingredient)
                                <option value="{{$ingredient->id}}">{{$ingredient->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" value="Guardar">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
