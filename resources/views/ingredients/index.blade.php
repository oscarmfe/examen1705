@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ingredientes</div>

                <div class="panel-body">                                
                @can ('create', \App\Ingredient::class)
                <a href="/ingredients/create">Alta de ingrediente</a>                
                @else                
                <!-- No puedes dar de alta :-( -->
                @endcan
                    
                    <table class="table">
                        <thead>
                            <th>Nombre</th>
                            <th>Tipo</th>
                        </thead>
                        @foreach ($ingredients as $ingredient)
                            <tr>
                                <td>{{$ingredient->name}}</td>

                                <td><a href="/ingredients/{{$ingredient->id}}">Ver más...</a></td>
                                <td>
                                    <form class="" action="/ingredients/{{$ingredient->id}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" value="Borrar" class="btn btn-danger">
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    </table>
                    {{ $ingredients->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
