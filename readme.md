# Desarrollo Web en entorno servidor. Grupo DAW2. Junio, parte 1 (Laravel)
## Entrega
- Toma fotos de la pantalla cada 5s con Chronollapse. 
- Déjalas en tu carpeta de red.
- Guarda tu código en bitbucket y compártelo con el profesor.
## Enunciado
Haz un FORK en el repositorio  git@bitbucket.org:rafacabeza/examen1705.git .  En él ya cuentas con:
1. La estructura base de Laravel 
2. Migración de las tablas y seeders. 
3. FALTA: modelos y métodos que reflejen las relaciones de clave ajena entre ellas. 
###  Desarrollar un proyecto de una pizzería. Con las siguientes tablas:
1. User (ya definida por Laravel). 
2. Pizza. Nombre de la pizza
3. Ingredients. Los usados para las pizzas.
4. Type. Tipos de ingredientes.  
5. Relaciones: User-Pizza (1:N), User-Ingredient (1:N), Ingredient-Pizza (N:M), Type-Ingredient (1:N).
### Ejercicios
1. Configura Laravel para comenzar a funcionar. Crea una base de datos examen1705. 
2. Añade los modelos necesarios y sus métodos de relaciones para que las migraciones y seeders funcionen correctamente. Ojo, se revisarán las 4 relaciones aunque todas no se usen en las migraciones.
3. Crea un controlador resource para gestionar la tabla de pizzas y otro de ingredientes.
4. '/pizzas'. Listado de Pizzas con paginación. Debe incluír el nombre del usuario que lo da de alta. (1 Punto) 
4. '/ingredients'. Listado de ingredientes con paginación. Debe incluír el nombre del usuario que lo da de alta y el nombre del tipo de ingrediente. (1 Punto) 
5. create/store. Alta de ingredientes. ¡¡¡El usuario es el logueado!!!. El género debe elegirse en un control select. (1 Punto). 
6. Validación del alta de libros con mensajes en español. (1 Punto). 
7. Show. La ruta GET '/pizzas/{id}' debe mostrar la información detallada de la pizza, incluidos su lista de ingredientes y el precio como suma de los precios de cada ingrediente.
8. delete. Borrado de libros. (1 Punto). 
8. Haz que sólo los usuarios logueados puedan acceder a la aplicación. (1 Punto)
9. 11. Uso de sesiones. En la lista de ingredientes, añade un link (GET '/ingredients/{id}/save, que permita almacenar una lista de ingredientes.
    - La ruta GET '/session' debe mostrar la lista de ingredientes. (1 Punto).
    - En la vista '/pizzas/{id}, añade un enlace a la ruta POST '/pizzas/{id}/ingredients' que guarde todos los ingredientes de dicha lista en la pizza actual.(1 Punto).
10. API REST básica del modelo Pizza. Crea el controlador correspondiente y la ruta o rutas necesarias en api.php. Debe contener los métodos index, show, store, delete y update. Debes comprobarlos con Postman (2 Puntos).