<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $fillable = ['id', 'name'];

    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient');
    }
    
    public function users()
    {
        return $this->belongTo('App\User');
    }
}
