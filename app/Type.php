<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function ingredients()
    {
        return $this->belongTo('App\Ingredient');
    }   
}
