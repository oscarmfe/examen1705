<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Pepe',
            'email' => 'pepe@gmail.com',
            'password' => bcrypt('pepe'),
            ]);
        DB::table('users')->insert([
            'name' => 'Juan',
            'email' => 'juan@gmail.com',
            'password' => bcrypt('juan'),
            ]);
        DB::table('users')->insert([
            'name' => 'Ana',
            'email' => 'ana@gmail.com',
            'password' => bcrypt('ana'),
            ]);
        DB::table('users')->insert([
            'name' => 'Yolanda',
            'email' => 'yolanda@gmail.com',
            'password' => bcrypt('ana'),
            ]);
    }
}
